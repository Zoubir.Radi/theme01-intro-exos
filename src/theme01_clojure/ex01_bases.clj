;; # Exercice 1 : Expressions et types de base

(ns theme01-clojure.ex01-bases
  (:use midje.sweet))


;; ## Question 1 : des nombres ...

(facts
  (and 
    
    (= (type 42) :_______________________)
    
    (= (type 42.2):______________________)
  
  	(= (+ 12 14 18) :_______)
  
    (= (+ 12 14 (* 4 8 2)) :_________)
  
  	(= (zero? (quot 3 2)) :__________)
    
    (= (zero? (rem 4 2)) :___________)
    
    (= (= 12.0 12) :_________)
    
    (= (== 12.0 12) :__________)
    
    (= (not= 12.0 12) :_________)
    
    (= (inc 41) :_________)
    
    (= (dec 43) :_________))
  
  => true)

;; ## Question 2 : des booléens ... et nil

(facts 
  (and
    
    (= (type true) :_______________)
  
    (= (if false "un" "deux") :____________)
 
    (= (if true "un" "deux")  :____________)
    
    (= (if 42 "un" "deux")  :____________)

    (= (if nil "un" "deux") :___________)
    
    (= (nil? nil) :__________)
    
    (= (nil? false) :__________)
    
    (= (when 42 "ouaye") :____________)
    
    (= (when (not 42) "ouaye") :____________)

    (= (when-not (not 42) "ouaye") :____________))
    
  => true)

;; ## Question 3 : symboles et keywords

(facts
  (and
    
    (= (symbol? 'blabla) :_________)
    
    (= (keyword? :blabla) :_________)
    
    (= (symbol? :blabla) :_________)
    
    (= (name 'blabla) :____________)
    
    (= (name :blabla) :____________))   
  
  => true)

;; ## Question 4 : chaînes de caractères

(facts
  (and
    
    (= (type "Hello le monde") :_______________________)
    
    (= (count "Hello le monde") :_______)
    
    (= (str (* 2 21) " = " '(2 * 21)) :____________________)
    
    (= (get "Hello le monde" 9) :_______)
    
    (= (get "Hello" 9) :_______)
    
    (= (subs "Hello le monde" 6) :_________________)
    
    (= (subs "Hello le monde" 3 7) :_______________)
   
    (= (clojure.string/replace "Hello le monde" "e" "u")

       :___________________________________________)
    

    (= (clojure.string/replace "Hello le monde" #"[aeiou]" "x")

       :___________________________________________)
    
    (= (clojure.string/replace "Hello le monde" #"[^aeiou]" "x")

       :___________________________________________))
  
  => true)


