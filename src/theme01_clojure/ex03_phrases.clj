;; # Exercice 3 : Phrase-o-matic

(ns theme01-clojure.ex03-phrases)


;; => #'theme01-clojure.ex03-phrases/ex03-phrases

(defn gen-article
  "Générateur d'article."
  []
  (rand-nth ["le" "la" "un" "une"]))

;; => #'theme01-clojure.ex03-phrases/gen-article
(repeatedly 10 gen-article)

;; => ("une" "un" "une" "un" "la" "la" "un" "la" "une" "la")
;; ## Question 1 : générateurs d'adjectifs et de noms

;; ## Question 2 : générateur de proposition nominale

;; ## Question 3 : générateur de proposition verbale

;; ## Question 4 : générateur de phases

;; ## Question 5 : phrase-o-matic++


